const layerOptions = [
    // RGBs
    {
        name: "s2l2a_2019_s1_RGB",
        property: "SENTINEL",
        mode: "RGB",
        season: "SPRING",
        year: "2019",
    },
    {
        name: "s2l2a_2018_s2_RGB",
        property: "SENTINEL",
        mode: "RGB",
        season: "SUMMER",
        year: "2018",
    },
    {
        name: "s2l2a_2019_s2_RGB",
        property: "SENTINEL",
        mode: "RGB",
        season: "SUMMER",
        year: "2019",
    },
    {
        name: "s2l2a_2018_s3_RGB",
        property: "SENTINEL",
        mode: "RGB",
        season: "AUTUMN",
        year: "2018",
    },
    {
        name: "s2l2a_2019_s3_RGB",
        property: "SENTINEL",
        mode: "RGB",
        season: "AUTUMN",
        year: "2019",
    },
    {
        name: "s2l2a_2018_s4_RGB",
        property: "SENTINEL",
        mode: "RGB",
        season: "WINTER",
        year: "2018",
    },
    {
        name: "s2l2a_2019_s4_RGB",
        property: "SENTINEL",
        mode: "RGB",
        season: "WINTER",
        year: "2019",
    },

    // Nirs
    {
        name: "s2l2a_2019_s1_B08_P25",
        property: "SENTINEL",
        mode: "NIR",
        season: "SPRING",
        year: "2019",
    },
    {
        name: "s2l2a_2018_s2_B08_P25",
        property: "SENTINEL",
        mode: "NIR",
        season: "SUMMER",
        year: "2018",
    },
    {
        name: "s2l2a_2019_s2_B08_P25",
        property: "SENTINEL",
        mode: "NIR",
        season: "SUMMER",
        year: "2019",
    },
    {
        name: "s2l2a_2018_s3_B08_P25",
        property: "SENTINEL",
        mode: "NIR",
        season: "AUTUMN",
        year: "2018",
    },
    {
        name: "s2l2a_2019_s3_B08_P25",
        property: "SENTINEL",
        mode: "NIR",
        season: "AUTUMN",
        year: "2019",
    },
    {
        name: "s2l2a_2018_s4_B08_P25",
        property: "SENTINEL",
        mode: "NIR",
        season: "WINTER",
        year: "2018",
    },
    {
        name: "s2l2a_2019_s4_B08_P25",
        property: "SENTINEL",
        mode: "NIR",
        season: "WINTER",
        year: "2019",
    },
  {
      name: "clay_tot_psa_norm",
      group: "clay_tot_psa_norm",
      property: "PREDICTIONS",
      alias: "clay_tot_psa"
  },
  {
      name: "db_od",
      group: "db_od",
      property: "PREDICTIONS",
      alias: "db_od"
  },
  {
      name: "ph_h2o",
      group: "ph_h2o",
      property: "PREDICTIONS",
      alias: "ph_h2o"
  },
  {
      name: "oc",
      group: "oc",
      property: "PREDICTIONS",
      alias: "oc"
  },
  {
      name: "ph_kcl",
      group: "ph_kcl",
      property: "PREDICTIONS",
      alias: "ph_kcl"
  },
  {
      name: "sand_tot_psa_norm",
      group: "sand_tot_psa_norm",
      property: "PREDICTIONS",
      alias: "sand_tot_psa"
  },
  {
      name: "silt_tot_psa_norm",
      group: "silt_tot_psa_norm",
      property: "PREDICTIONS",
      alias: "silt_tot_psa"
  },
  // {
  //     name: "dbr",
  //     property: "PREDICTIONS",
  //     alias: "dbr"
  // },
  {
      name: "k_mehlich3",
      group: "k_mehlich3",
      property: "PREDICTIONS",
      alias: "k_mehlich3"

  },
  {
      name: "p_mehlich3",
      group: "p_mehlich3",
      property: "PREDICTIONS",
      alias: "p_mehlich3"
  },
  {
      name: "ca_mehlich3",
      group: "ca_mehlich3",
      property: "PREDICTIONS",
      alias: "ca_mehlich3"
  },
  {
      name: "mg_mehlich3",
      group: "mg_mehlich3",
      property: "PREDICTIONS",
      alias: "mg_mehlich3"

  },
  {
      name: "caco3",
      group: "caco3",
      property: "PREDICTIONS",
      alias: "caco3"
  },
  {
      name: "wpg2",
      group: "wpg2",
      property: "PREDICTIONS",
      alias: "wpg2"
  },
  {
      name: "ec_satp",
      group: "ec_satp",
      property: "PREDICTIONS",
      alias: "ec_satp"
  },
  {
      name: "cec_sum",
      group: "cec_sum",
      property: "PREDICTIONS",
      alias: "cec_sum"
  },
  {
      name: "wrb_rsg",
      group: "wrb_rsg",
      property: "PREDICTIONS",
      alias: "wrb_rsg"
  },
  {
      name: "texture",
      group: "texture",
      property: "PREDICTIONS",
      alias: "texture_class"
  },
  {
      name: "clay_tot_psa_uncertainty",
      group: "clay_tot_psa_norm",
      property: "UNCERTAINTY",
      alias: "clay_tot_psa_uncertainty"
  },
  {
      name: "db_od_uncertainty",
      group: "db_od",
      property: "UNCERTAINTY",
      alias: "db_od_uncertainty"
  },
  {
      name: "ph_h2o_uncertainty",
      group: "ph_h2o",
      property: "UNCERTAINTY",
      alias: "ph_h2o_uncertainty"
  },
  {
      name: "oc_uncertainty",
      group: "oc",
      property: "UNCERTAINTY",
      alias: "oc_uncertainty"

  },
  {
      name: "ph_kcl_uncertainty",
      group: "ph_kcl",
      property: "UNCERTAINTY",
      alias: "ph_kcl_uncertainty"
  },
  {
      name: "sand_tot_psa_uncertainty",
      group: "sand_tot_psa_norm",
      property: "UNCERTAINTY",
      alias: "sand_tot_psa_uncertainty"
  },
  {
      name: "silt_tot_psa_uncertainty",
      group: "silt_tot_psa_norm",
      property: "UNCERTAINTY",
      alias: "silt_tot_psa_uncertainty"
  },
//   {
//       name: "dbr_uncertainty",
//       property: "UNCERTAINTY",
//       alias: "dbr_uncertainty"

//   },
  {
      name: "k_mehlich3_uncertainty",
      group: "k_mehlich3",
      property: "UNCERTAINTY",
      alias: "k_mehlich3_uncertainty"
  },
  {
      name: "p_mehlich3_uncertainty",
      group: "p_mehlich3",
      property: "UNCERTAINTY",
      alias: "p_mehlich3_uncertainty"
  },
  {
      name: "ca_mehlich3_uncertainty",
      group: "ca_mehlich3",
      property: "UNCERTAINTY",
      alias: "ca_mehlich3_uncertainty"
  },
  {
      name: "mg_mehlich3_uncertainty",
      group: "mg_mehlich3",
      property: "UNCERTAINTY",
      alias: "mg_mehlich3_uncertainty"
  },
  {
      name: "caco3_uncertainty",
      group: "caco3",
      property: "UNCERTAINTY",
      alias: "caco3_uncertainty"
  },
  {
      name: "wpg2_uncertainty",
      group: "wpg2",
      property: "UNCERTAINTY",
      alias: "wpg2_uncertainty"
  },
  {
      name: "ec_satp_uncertainty",
      group: "ec_satp",
      property: "UNCERTAINTY",
      alias: "ec_satp_uncertainty"
  },
  {
      name: "cec_sum_uncertainty",
      group: "cec_sum",
      property: "UNCERTAINTY",
      alias: "cec_sum_uncertainty"
  },
  {
      name: "wrb_rsg_prediction_entropy",
      group: "wrb_rsg",
      property: "UNCERTAINTY",
      alias: "wrb_rsg_uncertainty"
  },
]

export default layerOptions;
